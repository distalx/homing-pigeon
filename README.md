### Homing Pigeon

Email automation tool. (node-mailer + gmail)


#### Getting Started

- Go to https://myaccount.google.com/lesssecureapps and enable less secure apps.

- create a `.env` file in `root` directory and set values.

- install npm modules.
```
npm install
```

- send test email
```
node index.js test
```

- create a new list `welcome.csv` and place under `list` directory.
```
// welcome.csv
Dan, dan@example.com
Kira, kira@example.com
```

- create a email template `welcome.html` and place it under `templates` directory
( Sample email template can be found under templates directory.)

- send welcome message
```
node index.js send welcome
```
--------





to host images you can use `cloudinary.com`

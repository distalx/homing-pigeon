const envy = require('envy');
const path = require('path');
const fs = require('fs');
const parse = require('csv-parse');
const env = envy();
const handlebars = require('handlebars')
const nodemailer = require('nodemailer');

const transporter = nodemailer.createTransport({
 service: 'gmail',
 auth: {
        user: env.email,
        pass: env.password
    }
});


const args = process.argv.slice(2);

// args
const run_mode = args[0];

// check if args are there
if (!run_mode) {
  return console.log("Please select the mode. Either 'test' or 'send' ");
}


if (run_mode === 'test') {
  const email_type = 'sample_email';
  fs.readFile(`./templates/${email_type}.html`, 'utf-8', function(error, source){


    const template = handlebars.compile(source);


    const data = {
      name: 'Test User'
    }

    const html = template(data);

    const mailOptions = {
      from: `${env.name} <${env.email}>`, // sender address
      to: `${env.name} <${env.email}>`, // list of receivers
      subject: 'Test Email', // Subject line
      html: html
    };

    transporter.sendMail(mailOptions, function (err, info) {
       if(err)
        console.log(err)
       else
        console.log("emai sent to ", info.accepted);
    });


  });
}


if (run_mode === 'send') {


  if (!args[1]) {
    return console.log("Please provide a email type.");
  }

  const email_type = args[1];

  try {
      // Query the entry
      if (! fs.existsSync(`./lists/${email_type}.csv`)) {
        console.log(`${email_type} list does not exists`);

      }
      if (! fs.existsSync(`./templates/${email_type}.html`)) {
        console.log(`${email_type} email template`);
      }
  }
  catch (e) {
      console.log("Err ", e);

  }

  
  fs.createReadStream(`./lists/${email_type}.csv`)
    .pipe(parse({delimiter: ','}))
    .on('data', function(csvrow) {

      const c_name = csvrow[0];
      const c_email = csvrow[1];

      fs.readFile(`./templates/${email_type}.html`, 'utf-8', function(error, source){

        const template = handlebars.compile(source);

        const data = {
          name: c_name
        }
        const html = template(data);
        // console.log(html)

        const mailOptions = {
          from: `${env.name} <${env.email}>`, // sender address
          to: `${c_name} <${c_email}>`, // list of receivers
          subject: 'Subject of your email', // Subject line
          html: html
        };

        transporter.sendMail(mailOptions, function (err, info) {
           if(err)
            console.log(err)
           else
            console.log("emai sent to ", info.accepted);
        });


      });

    });

}
